import time
import os
import dask.dataframe as dd
from dask.distributed import Client
import multiprocessing


def generate_files(df_sub,sub_id,type):
    ### Criar os DATAFRAMES 
    df_trace = dd.read_csv(
    '/mnt/azure_trace_downloader/vm_cpu_readings-file-*.csv',
                                    dtype={"time":int,"vm_id": str, "min_usage":float, "avg_usage": float,"max_usage": float}
        
    )

    cores =  {
        'arOKfxGEFb/Gk4rbpN3yA8zNn19Goim1cUSpHjzRXYkC42eOinXN/SCjJ/pMLmc1':['8','4','2'],
        'xjOmJeOxvrHmYa5ljS1rc8blPa0GeyNEfIyzTCt258x20eNwAMheVrb7XA6mcCWC':['4','2']    
    }


    print('### PRIMEIRO OS BATCHS')    
    df_sub_batch = df_sub.loc[(df_sub["vm_category"] == 'Delay-insensitive')]
    for core in cores[sub_id]:
        df_sub_batch_cores = df_sub_batch.loc[(df_sub_batch["vm_virtual_core_count_bucket"] == core)][['vm_id']]
        j= 0
        for row in df_sub_batch_cores.iterrows():
            print(row[1][0])
            vm_id = row[1][0]
            print(f'processing VM {j} - {vm_id} cores {core}')
            df_1_task =  df_trace.loc[(df_trace["vm_id"] ==vm_id )].compute()[['time','min_usage', 'avg_usage','max_usage' ]]                        
            path =f"/home/msilvavasconcelos/azure_traces/{type}_data/batch/{core}/vm_{j}.csv/"
            os.makedirs(path)
            path = path+'*.csv'
            df_1_task.to_csv(path)
            j=j+1

    print('### DEPOIS OS SERVICES')    
    df_sub_service = df_sub.loc[(df_sub["vm_category"] == 'Interactive')]
    for core in cores[sub_id]:
        df_sub_service_cores = df_sub_service.loc[(df_sub_service["vm_virtual_core_count_bucket"] == core)][['vm_id']]
        j= 0
        for row in df_sub_service_cores.iterrows():
            print(row[1][0])
            vm_id = row[1][0]
            print(f'processing VM {j} - {vm_id} cores {core}')
            df_1_task =  df_trace.loc[(df_trace["vm_id"] ==vm_id )].compute()[['time','min_usage', 'avg_usage','max_usage' ]]                        
            path =f"/home/msilvavasconcelos/azure_traces/{type}_data/service/{core}/vm_{j}.csv/"
            os.makedirs(path)
            path = path+'*.csv'
            df_1_task.to_csv(path)
            j=j+1


def run():
    Client(n_workers=multiprocessing.cpu_count())



    df_vms = dd.read_csv(
    '/mnt/azure_trace_downloader/vmtable.csv',
    dtype={
                                            "vm_id":str,
                                            "subscription_id": str, 
                                            "deployment_id":str, 
                                            "timestamp_vm_created": int,
                                            "timestamp_vm_deleted": int,
                                            "max_cpu": float,
                                            "avg_cpu": float,
                                            "p95_max_cpu":float,
                                            "vm_category": str,
                                            "vm_virtual_core_count_bucket": str,
                                            "vm_memory_bucket": str
                                        }     
    )


    subs_ids = ['arOKfxGEFb/Gk4rbpN3yA8zNn19Goim1cUSpHjzRXYkC42eOinXN/SCjJ/pMLmc1','xjOmJeOxvrHmYa5ljS1rc8blPa0GeyNEfIyzTCt258x20eNwAMheVrb7XA6mcCWC']


    total_start = time.time()

        
    for i in range(0,len(subs_ids)):
        print('### First the training VMs')
        sub_id =  subs_ids[i] 

        df_sub = df_vms.loc[(df_vms["subscription_id"] == sub_id)].compute()    
        df_train = df_sub.loc[(df_sub["timestamp_vm_created"] <= 604800)]
        generate_files(df_train,sub_id,'test')
        
        print('### Now the validation VMs')
        ### Now the validation VMs:
        df_test = df_sub.loc[(df_sub["timestamp_vm_created"] > 604800)]
        generate_files(df_test,sub_id, 'validation')
        
        
    total_end = time.time()

    print(f'executed in {total_end-total_start} s ')


if __name__ == "__main__":
    run()
